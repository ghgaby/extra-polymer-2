## TEMARIO Polymer 2

- HTML/CSS3
  - https://www.bitdegree.org/learn/css3-features
  - https://www.bitdegree.org/learn/html5-tags
- GIT
  - Branching workflows for Git
    - https://medium.com/@patrickporto/4-branching-workflows-for-git-30d0aaee7bf
  - https://learngitbranching.js.org/
  - https://www.atlassian.com/es/git/tutorials/comparing-workflows
  - https://nvie.com/posts/a-successful-git-branching-model/
  - Semantic version
    - https://docs.npmjs.com/about-semantic-versioning
    - https://semver.npmjs.com/
  - https://github.com/lukehoban/es6features
- Polymer 2
  - https://polymer-library.polymer-project.org/2.0/docs/about_20
  - Ciclo de vida del componente
  - Custom event
  - **dom-repeat, dom-if**
  - *notify, observers, reflectToAttribute*
  - Estilos
  - Peticiones AJAX
- Theme
- Archivo de configuración
- Bridge

